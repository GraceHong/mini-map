# Map

Mini map, Road map system application

The main purpose of this project is to create a weighted & directed graph with speed limitation.
􏰂 * Used C/C++ dynamic data structure and algorithm to solve shortest path problems in directed weighted path with Application Programming Interface and testing in script language via calling the provided API by C/C++ application.
􏰂 * Developed skills of using C and C++ and gained more knowledge about data structure.
􏰂 * Gained skills and knowledge about making high performance TCP based service.

