//
// Created by Morgan Wu on 11/25/16.
//

#include <fstream>
#include "WatMap.h"
#include <stack>

using namespace std;
using namespace mymap;

/**
 * delimiter is "|"
 */
vector<string> *split(string source) {
    vector<string> *result = new vector<string>;
    size_t found = source.find_first_of("|");
    string data = "";
    while (found != string::npos) {
        data = source.substr(0, found);
        result->push_back(data);
        source = source.substr(found + 1);
        found = source.find_first_of("|");
    }
    result->push_back(source);
    return result;
}


WatMap::WatMap() {
    vertices = new map<int, Vertex *>; // store vertices
    edges = new map<int, Edge *>;
    roads = new vector<Road *>;
    adjListMap = new map<int, vector<Edge *> *>; // vertexId -> edges started from vertexId
}

WatMap::~WatMap() {
    delete vertices;
    delete edges;
    delete roads;
    delete adjListMap;
}

Edge *WatMap::getEdge(int edgeId) {
    map<int, Edge *>::iterator it = edges->find(edgeId);
    if (it == edges->end()) { // not found
        return NULL;
    } else {
        return it->second;
    }
}

vector<Edge *> *WatMap::getAdjEdges(int vertexId) {
    map<int, vector<Edge *> *>::iterator it = adjListMap->find(vertexId);
    if (it == adjListMap->end()) { // not found
        return NULL;
    } else {
        return it->second;
    }
}

vector<Edge *> *WatMap::getInEdges(int vertexId) {
    vector<Edge *> *result = new vector<Edge *>;
    for (auto e: *this->edges) {
        if (e.second->end->vertexId == vertexId) {
            result->push_back(e.second);
        }
    }
    return result;
}

Edge *WatMap::checkEdge(int fromVertexId, int endVertexId) {
    vector<Edge *> *edges = this->getAdjEdges(fromVertexId);
    if(edges == NULL) return NULL;
    for (auto e: *edges) {
        if (endVertexId == e->end->vertexId)
            return e;
    }
    return NULL;
}

Vertex *WatMap::getVertex(int vertexId) {
    map<int, Vertex *>::iterator it = vertices->find(vertexId);
    if (it == vertices->end()) { // not found
        return NULL;
    } else {
        return it->second;
    }
}

int WatMap::addVertex(string type, string label) {
    int vertexId = vertices->size();
    Vertex *v = new Vertex(type, label, vertexId);
    vertices->insert(pair<int, Vertex *>(vertexId, v));
    return vertexId;
}

void WatMap::insertVertex(string type, string label, int vertexId) {
    Vertex *v = new Vertex(type, label, vertexId);
    vertices->insert(pair<int, Vertex *>(vertexId, v));
}

int *WatMap::addEdge(int vertex1, int vertex2, bool directional, double speed, int length) {
    int *addedEdges = new int[2]{-1, -1};
    Vertex *from = this->getVertex(vertex1);
    Vertex *end = this->getVertex(vertex2);
    if (from == NULL) {
        cerr << "Unable to find vertex " << from->vertexId << endl;
    }
    if (end == NULL) {
        cerr << "Unable to find vertex " << end->vertexId << endl;
    }
    Edge *edge = new Edge(from, end, speed, length, edges->size());
    edges->insert(pair<int, Edge *>(edge->edgeId, edge));
    map<int, vector<Edge *> *>::iterator edgesIterator = adjListMap->find(vertex1);
    if (edgesIterator == adjListMap->end()) { // not found
        vector<Edge *> *adjEdges = new vector<Edge *>;
        adjEdges->push_back(edge);
        adjListMap->insert(pair<int, vector<Edge *> *>(vertex1, adjEdges));
    } else {
        vector<Edge *> *edges = edgesIterator->second;
        for (auto e: *edges) {
            if (e->end->vertexId == vertex2) {
                cerr << "edges from " << vertex1 << " to " << vertex2 << " already exist " << endl;
                return addedEdges;
            }
        }
        edges->push_back(edge);
    }
    addedEdges[0] = edge->edgeId;
    if (!directional) {  // bi-directional, add reverse edge
        int *tempEdges = addEdge(vertex2, vertex1, true, speed, length);
        addedEdges[1] = tempEdges[0];
    }
    return addedEdges;
}

/**
 * if can't find return -1
 */
int WatMap::vertex(string point_of_interest) {
    for (auto v: *vertices) {
        if (v.second->label == point_of_interest) {
            return v.first;
        }
    }
    return -1;
}

void WatMap::road(int *edges) {
    if (edges == NULL) {
        cerr << "edges can't be NULL!" << endl;
        return;
    }
    Road *r = new Road;
    int edgeLength = sizeof(edges) / sizeof(*edges);
    for (int i = 0; i < edgeLength; i++) {
        Edge *edge = this->getEdge(edges[i]);
        if (edge == NULL) {
            cerr << "Unable to find edge: " << edge->edgeId << endl;
        }
        r->addEdge(edge);
    }
    roads->push_back(r);
}

void WatMap::edgeEvent(int edge, string eventType) {
    Edge *e = this->getEdge(edge);
    if (e == NULL) {
        cerr << "Unable to find edge with ID: " << e->edgeId << endl;
        return;
    }
    if (eventType == "construction") {
        e->speed = e->speed * 0.3;
    }
    else if (eventType == "accident") {
        e->speed = e->speed * 0.6;
    }
}

Vertex *WatMap::pickUpUnvisitedConnectedVertex(vector<Vertex *> *visited, vector<Vertex *> *unvisited) {
    for (auto v_target: *unvisited) {
        for (auto v_source: *visited) {
            if (this->checkEdge(v_source->vertexId, v_target->vertexId) != NULL) {
                return v_target;
            }
        }
    }
    return NULL;
}

vector<Vertex *> *removeVertex(vector<Vertex *> *unvisited, Vertex *vertex) {
    vector<Vertex *> *result = new vector<Vertex *>;
    for (auto v: *unvisited) {
        if (v->vertexId != vertex->vertexId) {
            result->push_back(v);
        }
    }
    return result;
}

int *WatMap::trip(int fromVertex, int toVertex) {
    map<int, int> prev;
    map<int, double> dist;
    vector<Vertex *> *visited = new vector<Vertex *>;
    vector<Vertex *> *unvisited = new vector<Vertex *>;
    for (auto v: *vertices) {
        prev.insert(pair<int, int>(v.first, -1));
        dist.insert(pair<int, double>(v.first, 1000000000));
        unvisited->push_back(v.second);
    }
    Vertex *current_vertex = getVertex(fromVertex);
    visited->push_back(current_vertex);
    unvisited = removeVertex(unvisited, current_vertex);
    dist.at(fromVertex) = 0;
    prev.at(fromVertex) = current_vertex->vertexId;
    while ((current_vertex = pickUpUnvisitedConnectedVertex(visited, unvisited)) != NULL) {
        vector<Edge *> *inEdges = this->getInEdges(current_vertex->vertexId);
        for (auto inEdge: *inEdges) {
            if ((dist.at(inEdge->start->vertexId) + (inEdge->length / inEdge->speed)) < dist.at(current_vertex->vertexId)) {
                dist.at(current_vertex->vertexId) = dist.at(inEdge->start->vertexId) + (inEdge->length / inEdge->speed);
                prev.at(current_vertex->vertexId) = inEdge->start->vertexId;
            }
        }
        visited->push_back(current_vertex);
        unvisited = removeVertex(unvisited, current_vertex);
    }
    stack<int> *resultstack = new stack<int>;
    resultstack->push(toVertex);
    int _vertexId = toVertex;
    while (_vertexId != fromVertex) {
        int _prev = prev.at(_vertexId);
        _vertexId = _prev;
        resultstack->push(_vertexId);
    }
    vector<int> *result = new vector<int>;
    while (!resultstack->empty()) {
        result->push_back(resultstack->top());
        resultstack->pop();
    }
    result->push_back(-1);
    return result->data();
}

void WatMap::store(string filename) {
    ofstream file;
    file.open(filename);
    file << "// vertex, id|type|label" << endl;
    for (auto v: *vertices) {
        Vertex *vertex = v.second;
        file << "VERT:" << vertex->vertexId << "|" << vertex->type << "|" << vertex->label << endl;
    }
    file << "" << endl;
    file << "// edge: id|start|end|speed|length" << endl;
    for (auto e: *edges) {
        Edge *edge = e.second;
        file << "EDGE:" << edge->edgeId << "|" << edge->start->vertexId << "|" << edge->end->vertexId << "|" << edge->speed << "|" << edge->length << endl;
    }
    file.close();
}

WatMap *WatMap::retrieve(string filename) {
    WatMap *map = new WatMap;
    ifstream reader;
    reader.open(filename);
    string line;
    while (getline(reader, line)) {
        if (line.find("VERT") == 0 || line.find("EDGE") == 0) { // don't find a "//"
            string start = line.substr(0, 4);
            string data = line.substr(5);
            vector<string> *v_line = split(data);
            if (start == "VERT") {
                int vertexId = stoi(v_line->at(0));
                string type = v_line->at(1);
                string label = v_line->at(2);
                map->insertVertex(type, label, vertexId);
            }
            else if (start == "EDGE") {
                int edgeId = stoi(v_line->at(0));
                int startVertexId = stoi(v_line->at(1));
                int endVertexId = stoi(v_line->at(2));
                double speed = stod(v_line->at(3));
                int length = stoi(v_line->at(4));
                map->addEdge(startVertexId, endVertexId, true, speed, length);
            }
        }
    }

    reader.close();
    return map;
}

void WatMap::printVertices() {
    for (auto v: *vertices) {
        cout << "Vertex Id: " << v.first << ", Vertex Label: " << v.second->label << endl;
    }
}

void WatMap::printEdges() {
    for (auto e: *this->edges) {
        Edge *edge = e.second;
        cout << edge->getString() << endl;
    }
}

void WatMap::printMap() {
    this->printVertices();
    this->printEdges();
}

int main1(int argc, char *argv[]) {
    WatMap *map = WatMap::retrieve("./data.txt");
    map->printMap();
    int *records1 = map->trip(0, 7);
    int v1 = *records1, index1 = 0;
    cout << "The shortest path is: ";
    while (v1 != -1) {
        cout << map->getVertex(v1)->label << " => ";
        v1 = *(records1 + index1);
        index1++;
    }
    cout << endl;
    exit(1);
    map->printMap();
    map->store("./data.txt");

    int vertexId1 = map->addVertex("point-of-interest", "Waterloo SLC");
    int vertexId2 = map->addVertex("point-of-interest", "E3");
    int vertexId3 = map->addVertex("intersection", "E2");
    int *edges = map->addEdge(vertexId1, vertexId2, false, 50.0, 100);
//    map->addEdge(vertexId1, vertexId3, false, 50.0, 100);
    map->addEdge(vertexId2, vertexId3, false, 50.0, 100);
    map->edgeEvent(edges[0], "construction");
    map->edgeEvent(edges[1], "accident");
    map->printMap();
    cout << map->getEdge(edges[0])->getString() << endl;
    cout << map->getEdge(edges[1])->getString() << endl;
    int vertexId = map->vertex("EIT");
    cout << vertexId << endl;
    int *records = map->trip(0, 2);

    int v = *records, index;
    cout << "The shortest path is: ";
    while (v != -1) {
        cout << map->getVertex(v)->label << " => ";
        v = *(records + index);
        index++;
    }
    cout << endl;
    return 0;
}
