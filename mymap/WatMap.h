//
// Created by Morgan Wu on 11/25/16.
//

#ifndef ASSIGNMENT3_WATMAP_H
#define ASSIGNMENT3_WATMAP_H

#define DEBUG 1

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>

using namespace std;

namespace mymap {
    class Vertex {
    public:
        string type;
        string label;
        int vertexId;

        Vertex(const string &type, const string &label, int vertexId) : type(type), label(label), vertexId(vertexId) { }
    };

    class Edge {
    public:
        Vertex *start;
        Vertex *end;
        double speed; // km/h
        int length; // meters
        int edgeId;

        Edge(Vertex *start, Vertex *end, double speed, int length, int edgeId) : start(start), end(end), speed(speed), length(length), edgeId(edgeId) { }

        string getString() {
            stringstream sstream;
            sstream << "edgeId: " << edgeId << ", " << start->label << " ======> " << end->label << ", speed: " << speed << ", length: " << length << ". ";
            return sstream.str();
        }
    };

    class Road {
    public:
        vector<Edge *> *edges;

        Road() {
            this->edges = new vector<Edge *>;
        }

        void addEdge(Edge *edge) {
            this->edges->push_back(edge);
        }
    };

    class WatMap {
    private:
        map<int, Vertex *> *vertices; // store vertices
        map<int, Edge *> *edges; // store the edges
        vector<Road *> *roads; // store roads
        map<int, vector<Edge *> *> *adjListMap; // vertexId -> edges started from vertexId

    public:
        WatMap();

        ~WatMap();

        Edge *getEdge(int edgeId);

        Vertex *getVertex(int vertexId);

        vector<Edge *> *getAdjEdges(int vertexId);

        vector<Edge *> *getInEdges(int vertexId);


        /**
         * if edge exist, then return
         */
        Edge *checkEdge(int fromVertexId, int endVertexId);

        Vertex *pickUpUnvisitedConnectedVertex(vector<Vertex *> *visited, vector<Vertex *> *unvisited);

        /**
         * type: point-of-interest/intersection, create a new vertex with id as the size of vertices
         */
        int addVertex(string type, string label);

        /**
         * insert vertex with a specified veretxId
         */
        void insertVertex(string type, string label, int vertexId);

        /**
         * if not directional, should add both edges
         */
        int *addEdge(int vertex1, int vertex2, bool directional, double speed, int length);

        /**
         * edge: edgeIndex, eventType: construction|accident, will cause: 1) lower speed, unit: km/h; 2) closed
         */
        void edgeEvent(int edge, string eventType);

        /**
         * edge index list
         */
        void road(int edges[]);

        /**
         * computed by dijkstra, return vertex Id list, end with -1
         */
        int *trip(int fromVertex, int toVertex);

        /**
         * get the vertexId
         */
        int vertex(string point_of_interest);

        /**
         * serialize
         */
        void store(string filename);

        /**
         * de-serialize
         */
        static WatMap *retrieve(string filename);

        void printVertices();

        void printEdges();

        void printMap();

    };
}

#endif //ASSIGNMENT3_WATMAP_H
