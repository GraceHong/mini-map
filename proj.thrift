namespace cpp mymapservice

enum Operation {
  ADD_VERETX = 1, // add a vertex
  ADD_EDGE = 2, // add an edge
  EDGE_EVENT = 3, // specify an edge event
  ADD_ROAD = 4, //add a road by using edge list
  TRIP = 5, // trip from-to
  VERTEX_ID = 6, // get the vertex id
  VERTEX_LABEL = 7, // get the vertex label
  STORE = 8, // store the map
  RETRIEVE = 9, // retrieve the map
  PRINT_MAP = 10; // print the map
}

struct AddVertexRequest {
  1: string type,
  2: string label,
  3: string op_username,
  4: string op_password
}

// int vertex1, int vertex2, bool directional, double speed, int length
struct AddEdgeRequest {
  1: i32 vertex1,
  2: i32 vertex2,
  3: bool directional,
  4: double speed,
  5: i32 length,
  6: string op_username,
  7: string op_password
}

// int edge, string eventType
struct EdgeEventRequest {
  1: i32 edge,
  2: string eventType,
  3: string op_username,
  4: string op_password
}

// int *edges
struct AddRoadRequest {
  1: list<i32> edges,
  2: string op_username,
  3: string op_password
}

// int fromVertex, int toVertex
struct TripRequest {
  1: i32 fromVertex,
  2: i32 toVertex
}

// string point_of_interest
struct VertexIdRequest {
  1: string point_of_interest
}

struct VertexLabelRequest {
  1: i32 vertexId
}

struct StoreRequest {
  1: string op_username,
  2: string op_password
}

struct RetrieveRequest {
  1: string op_username,
  2: string op_password
}

struct PrintMapRequest {
  1: string op_username,
  2: string op_password
}

exception InvalidOperation {
  1: Operation op,
  2: string why
}

service WatMapService {
  i32 addVertex_(1:AddVertexRequest request),
  list<i32> addEdge_(1:AddEdgeRequest request),
  void edgeEvent_(1:EdgeEventRequest request),
  void road_(1:AddRoadRequest request),
  list<i32> trip_(1:TripRequest request) throws (1:InvalidOperation ouch),
  i32 vertex_(1:VertexIdRequest request),
  string getVertex_(1:VertexLabelRequest request),
  void store_(1:StoreRequest request),
  void retrieve_(1:RetrieveRequest request),
  string printMap_(1:PrintMapRequest request)
}

