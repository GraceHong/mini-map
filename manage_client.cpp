#include "./gen-cpp/WatMapService.h"
#include <iostream>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TCompactProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>


using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace mymapservice;

int main() {
    boost::shared_ptr<TSocket> socket(new TSocket("localhost", 9090));
    boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
    boost::shared_ptr<TProtocol> protocol(new TCompactProtocol(transport));

    WatMapServiceClient client(protocol);
    try {
        transport->open();

        RetrieveRequest request;
        request.op_username = "admin";
        request.op_password = "admin";
        client.retrieve_(request);
        PrintMapRequest printMapRequest;
        printMapRequest.op_username = "admin";
        printMapRequest.op_password = "admin";
        string result;
        client.printMap_(result, printMapRequest);
        cout << result << endl;
        transport->close();
    } catch (TException &tx) {
        cout << "ERROR: " << tx.what() << endl;
    }

    return 0;
}