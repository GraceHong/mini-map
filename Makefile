#THRIFT_VER =thrift/0.9.3
#USR_DIR    =/home/vagrant
#THRIFT_DIR =${USR_DIR}/${THRIFT_VER}
#INCS_DIRS  =-I${USR_DIR}/include -I${THRIFT_DIR}/include/thrift -I${USR_DIR}/boost/1.62.0/include/boost
#LIBS_DIRS  =-L${USR_DIR}/${THRIFT_VER}/lib
CPP_DEFS   =-D=HAVE_CONFIG_H
CPP_OPTS   =-std=c++11 -Wall -O2
LIBS       =-lthrift

GEN_SRC    = ./gen-cpp/WatMapService.cpp \
             ./gen-cpp/proj_constants.cpp \
             ./gen-cpp/proj_types.cpp
MAP_SRC    = ./mymap/WatMap.cpp
GEN_INC    = -I./gen-cpp -I./mymap


default: thrift map_server map_client manage_client

thrift: 
	thrift -r --gen cpp proj.thrift 

map_server: server.cpp
	g++ ${CPP_OPTS} ${CPP_DEFS} -o map_server ${GEN_INC} ${MAP_SRC} ${INCS_DIRS} server.cpp ${GEN_SRC} ${LIBS_DIRS} ${LIBS}

map_client: client.cpp
	g++ ${CPP_OPTS} ${CPP_DEFS} -o map_client ${GEN_INC} ${MAP_SRC} ${INCS_DIRS} client.cpp ${GEN_SRC} ${LIBS_DIRS} ${LIBS}

manage_client: manage_client.cpp
	g++ ${CPP_OPTS} ${CPP_DEFS} -o manage_client ${GEN_INC} ${MAP_SRC} ${INCS_DIRS} manage_client.cpp ${GEN_SRC} ${LIBS_DIRS} ${LIBS}

clean:
	$(RM) -r map_client manage_client map_server

