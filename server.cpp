#include "WatMapService.h"
#include "WatMap.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TCompactProtocol.h>
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/PosixThreadFactory.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::concurrency;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using namespace mymapservice;
using namespace mymap;

const string DATA_FILE = "/Users/morganwu/Google Drive/U-Waterloo/ece-650-paul/ece650_proj/data.txt";

class WatMapServiceHandler : virtual public WatMapServiceIf {
public:
    WatMapServiceHandler() {
        // Your initialization goes here
        this->watMap = WatMap::retrieve(DATA_FILE);
    }

    int32_t addVertex_(const AddVertexRequest &request) {
        if (request.op_username == "admin" && request.op_password == "admin")
            return this->watMap->addVertex(request.type, request.label);
        return -1;
    }

    void addEdge_(std::vector<int32_t> &_return, const AddEdgeRequest &request) {
        std::vector<int32_t> result;
        if (request.op_username == "admin" && request.op_password == "admin") {
            int *edge = this->watMap->addEdge(request.vertex1, request.vertex2, request.directional, request.speed, request.length);
            if (request.directional) {
                result.push_back(edge[0]);
            } else {
                result.push_back(edge[0]);
                result.push_back(edge[1]);
            }
        }
        _return = result;
    }

    void edgeEvent_(const EdgeEventRequest &request) {
        if (request.op_username == "admin" && request.op_password == "admin")
            this->watMap->edgeEvent(request.edge, request.eventType);
    }

    void road_(const AddRoadRequest &request) {
        if (request.op_username == "admin" && request.op_password == "admin") {
            int edgeList[request.edges.size()];
            int index = 0;
            for (auto e: request.edges) {
                edgeList[index++] = e;
            }
            this->watMap->road(edgeList);
        }
    }

    void trip_(std::vector<int32_t> &_return, const TripRequest &request) {
        int *tripVertexIdList = this->watMap->trip(request.fromVertex, request.toVertex);
        std::vector<int32_t> result;
        int it = tripVertexIdList[0], index = 0;
        while (it != -1) {
            result.push_back(it);
            it = tripVertexIdList[++index];
        }
        _return = result;
    }

    int32_t vertex_(const VertexIdRequest &request) {
        return this->watMap->vertex(request.point_of_interest);
    }

    void getVertex_(std::string &_return, const VertexLabelRequest &request) {
        _return = this->watMap->getVertex(request.vertexId)->label;
    }

    void store_(const StoreRequest &request) {
        if (request.op_username == "admin" && request.op_password == "admin")
            this->watMap->store(DATA_FILE);
    }

    void retrieve_(const RetrieveRequest &request) {
        if (request.op_username == "admin" && request.op_password == "admin")
            this->watMap = WatMap::retrieve(DATA_FILE);
    }

    void printMap_(std::string &_return, const PrintMapRequest &request) {
        std::stringstream buffer;
        std::streambuf *old = std::cout.rdbuf(buffer.rdbuf());
        this->watMap->printMap();
        std::string text = buffer.str();
        std::cout.rdbuf(old);
        _return = text;
    }

private :
    WatMap *watMap;
};

int main(int argc, char **argv) {
    int port = 9090;

    boost::shared_ptr<WatMapServiceHandler> handler(new WatMapServiceHandler());
    boost::shared_ptr<TProcessor> processor(new WatMapServiceProcessor(handler));
    boost::shared_ptr<TProtocolFactory> protocolFactory(new TCompactProtocolFactory());
    boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
    boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));

    boost::shared_ptr<ThreadManager> threadManager = ThreadManager::newSimpleThreadManager(10);
    boost::shared_ptr<PosixThreadFactory> threadFactory = boost::shared_ptr<PosixThreadFactory>(new PosixThreadFactory());
    threadManager->threadFactory(threadFactory);
    threadManager->start();
    printf("start map servedr on port 9090...\n");

    TThreadPoolServer server(processor, serverTransport, transportFactory, protocolFactory, threadManager);
    server.serve();
    return 0;
}

