#include <iostream>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TCompactProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include "./gen-cpp/WatMapService.h"


using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace mymapservice;

int main() {
    boost::shared_ptr<TSocket> socket(new TSocket("localhost", 9090));
    boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
    boost::shared_ptr<TProtocol> protocol(new TCompactProtocol(transport));

    WatMapServiceClient client(protocol);
    try {
        transport->open();

        VertexIdRequest fromVertex;
        fromVertex.point_of_interest = "V1";
        VertexIdRequest toVertex;
        toVertex.point_of_interest = "V8";

        TripRequest request;
        request.fromVertex = client.vertex_(fromVertex);
        request.toVertex = client.vertex_(toVertex);
        vector<int32_t> vertexIdList;
        client.trip_(vertexIdList, request);
        for(auto id: vertexIdList) {
            VertexLabelRequest request;
            request.vertexId = id;
            string label;
            client.getVertex_(label, request);
            cout << label << " ==> " ;
        }
        cout << endl;

        transport->close();
    } catch (TException &tx) {
        cout << "ERROR: " << tx.what() << endl;
    }

    return 0;
}